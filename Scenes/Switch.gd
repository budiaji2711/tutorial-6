extends Interactable
export var light : NodePath
export var light2 : NodePath
export var light3 : NodePath
export var on_by_default = true
export var energy_when_on = 6
export var energy_when_off = 0
onready var light_node = get_node(light)
onready var light_node2 = get_node(light2)
onready var light_node3 = get_node(light3)
var on = on_by_default

func _ready():
	light_node.set_param(Light.PARAM_ENERGY, energy_when_on)
	light_node2.set_param(Light.PARAM_ENERGY, energy_when_on)
	light_node3.set_param(Light.PARAM_ENERGY, energy_when_on)
func interact():
	on = !on
	light_node.set_param(Light.PARAM_ENERGY, energy_when_on if on else energy_when_off)
	light_node2.set_param(Light.PARAM_ENERGY, energy_when_on if on else energy_when_off)
	light_node3.set_param(Light.PARAM_ENERGY, energy_when_on if on else energy_when_off)
